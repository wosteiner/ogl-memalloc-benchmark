# ogl-memalloc-benchmark

To run a single benchmark start `benchmark.exe` with:

* with-sample_alloc-empty_until-oom
* with-sample_alloc-data_until-oom
* no-sample_alloc-empty_until-oom
* no-sample_alloc-data_until-oom

To run all benchmarks start `runner.exe`.

Resulting .csv files will be stored in the exe directory.

# Driver crash on RTX 2080 / GTX 1080

✔️ Result-1-OK:  uses dedicated VRAM, then system RAM, then raises OpenGL error callback with OOM message  
✔️ Result-2-OK:  uses no dedicated VRAM and no system RAM, eventually raises OpenGL error callback with OOM message  
❌ Result-3-NOK: uses dedicated VRAM, then crashes in the driver as soon as the physical VRAM is depleted

### Test results GTX 980 Ti:
* with-sample_alloc-empty_until-oom -> ✔️ Result-1-OK
* with-sample_alloc-data_until-oom -> ✔️ Result-1-OK
* no-sample_alloc-empty_until-oom -> ✔️ Result-2-OK
* no-sample_alloc-data_until-oom -> ✔️ Result-1-OK

### Test results GTX 1080:
* with-sample_alloc-empty_until-oom -> ❌ Result-3-NOK
* with-sample_alloc-data_until-oom -> ❌ Result-3-NOK
* no-sample_alloc-empty_until-oom -> ✔️ Result-2-OK
* no-sample_alloc-data_until-oom -> ✔️ Result-1-OK

### Test results RTX 2080:
* with-sample_alloc-empty_until-oom -> ❌ Result-3-NOK
* with-sample_alloc-data_until-oom -> ❌ Result-3-NOK
* no-sample_alloc-empty_until-oom -> ✔️ Result-2-OK
* no-sample_alloc-data_until-oom -> ✔️ Result-1-OK

### Windows Event-Log showing the crash on RTX 2080 / GTX 1080:
```
Name der fehlerhaften Anwendung: benchmark.exe, Version: 0.0.0.0, Zeitstempel: 0x5cb988f0
Name des fehlerhaften Moduls: nvoglv64.dll, Version: 25.21.14.2531, Zeitstempel: 0x5cac820e
Ausnahmecode: 0xc0000409
Fehleroffset: 0x0000000000ee5de9
ID des fehlerhaften Prozesses: 0xe20
Startzeit der fehlerhaften Anwendung: 0x01d50c8f674226a9
Pfad der fehlerhaften Anwendung: C:\code\news\ogl-memalloc-benchmark\build\Debug\benchmark.exe
Pfad des fehlerhaften Moduls: C:\Windows\System32\DriverStore\FileRepository\nv_dispi.inf_amd64_37ec54c19854e219\nvoglv64.dll
Berichtskennung: 0e0d6e76-0dee-45dc-bad6-15f8fe76eb61
Vollständiger Name des fehlerhaften Pakets: 
Anwendungs-ID, die relativ zum fehlerhaften Paket ist: 
```
and
```
Die Beschreibung für die Ereignis-ID "1" aus der Quelle "NVIDIA OpenGL Driver" wurde nicht gefunden. Entweder ist die Komponente, die dieses Ereignis auslöst, nicht auf dem lokalen Computer installiert, oder die Installation ist beschädigt. Sie können die Komponente auf dem lokalen Computer installieren oder reparieren.

Falls das Ereignis auf einem anderen Computer aufgetreten ist, mussten die Anzeigeinformationen mit dem Ereignis gespeichert werden.

Die folgenden Informationen wurden mit dem Ereignis gespeichert: 

An application has requested more GPU memory than is
available in the system.
The application will now be closed.


Error code: 6
 (pid=3616 tid=50220 benchmark.exe 64bit)

Visit http://nvidia.custhelp.com/app/answers/detail/a_id/3553 for more information.

Die Nachrichtenressource ist vorhanden, die Nachricht wurde in der Nachrichtentabelle jedoch nicht gefunden
```

# Benchmark results & charts for all tested graphics cards

https://docs.google.com/spreadsheets/d/1mSqUF5zsqFYlr_7vrODrIqIM2zD_-ftU__N8-GhUwxU/edit?usp=sharing
