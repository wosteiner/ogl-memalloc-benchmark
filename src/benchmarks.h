#pragma once

#include <chrono>

#include "gpu_utils.h"

class IBenchmark
{
public:
    uint32_t max_steps = 0;
    int step = 0;

    virtual ~IBenchmark() = default;

    virtual std::string getId() = 0;

    void showStep(int currStep, int maxSteps, std::string suffix = "")
    {
        auto title = "Step: " + std::to_string(currStep) + "/" + std::to_string(maxSteps) + " - " + getId();

        if (!suffix.empty())
            title += " - " + suffix;

        SetWindowTextA(hwnd, title.c_str());
    }

    void logInit()
    {
        const auto device = (const char*)glGetString(GL_RENDERER);
        const auto version = (const char*)glGetString(GL_VERSION);

        double vram = getTotalVRAM() * 1024;
        auto sys_stats = getSysStats();

        std::cout << getId() << " @ " << device << std::endl;
        std::cout << "DRIVER: " << version << std::endl;
        std::cout << "VRAM_AVAIL: " << formatMemory(vram) << " (" << vram << ")" << std::endl;
        std::cout << "SYS_MEM_AVAIL: " << formatMemory(sys_stats.totalBytes) << " (" << sys_stats.totalBytes << ")" << std::endl;
        std::cout << std::endl;

        std::cout << "TEX_MEM;TEX_MEM;";
        std::cout << "DEDICATED_MEM;DEDICATED_MEM;";
        std::cout << "SHARED_MEM;SHARED_MEM;";
        std::cout << "SYS_MEM_USED;SYS_MEM_USED;";
        std::cout << "FRAMES_uS;";
        std::cout << std::endl;
    }

    void logMemory(int currStep, int maxSteps)
    {
        auto gpu_luid = getGlContextGpuLuid();
        auto query_results = getHardwareQuery({ GPU_DedicatedMemoryEx(gpu_luid), GPU_SharedMemoryEx(gpu_luid) }, gpu_luid);
        auto sys_stats = getSysStats();

        const auto device = (const char*)glGetString(GL_RENDERER);
        std::stringstream sstr;

        if (query_results.empty() || sys_stats.empty())
        {
            std::cout << "ERROR QUERYING PROCESS / SYSTEM STATISTICS" << std::endl;
            sstr << " @ " << device;
            showStep(currStep, maxSteps, sstr.str());
        }
        else
        {
            double tex_mem = textures.size() * tex_size_px * tex_size_px * 4ull;

            std::cout << tex_mem << ";" << formatMemory(tex_mem) << ";";
            std::cout << query_results[0] << ";" << formatMemory(query_results[0]) << ";";
            std::cout << query_results[1] << ";" << formatMemory(query_results[1]) << ";";
            std::cout << sys_stats.usedBytes << ";" << formatMemory(sys_stats.usedBytes) << ";";
            std::cout << total_step_frame_time << ";";
            std::cout << std::endl;

            sstr <<
                "GPU T: " << formatMemory(tex_mem) <<
                " D: " << formatMemory(query_results[0]) <<
                " S: " << formatMemory(query_results[1]) <<
                " | SYS: " << formatMemory(sys_stats.usedBytes) <<
                " / " << formatMemory(sys_stats.totalBytes) <<
                "";
            sstr << " @ " << device;
            showStep(currStep, maxSteps, sstr.str());
        }

        total_step_frame_time = 0; // reset
    }

    std::chrono::high_resolution_clock timer;
    std::chrono::steady_clock::time_point frame_start_time;
    std::chrono::steady_clock::time_point frame_end_time;
    long long total_step_frame_time = 0;

    virtual void init() = 0;
    virtual void beforeFrame(const uint64_t& frame)
    {
        frame_start_time = timer.now();
    };
    virtual void afterFrame(const uint64_t& frame)
    {
        frame_end_time = timer.now();
        total_step_frame_time += std::chrono::duration_cast<std::chrono::microseconds>(frame_end_time - frame_start_time).count();

        if (textures.size() > max_steps)
        {
            std::cout << "EXIT_FINISHED" << std::endl;
            exit(0);
        }

        for (auto& msg : log_messages)
        {
            std::cout << msg << std::endl;

            if (msg.find("out of memory") != std::string::npos // nvidia
                || msg.find("GL_OUT_OF_MEMORY") != std::string::npos // intel
            )
            {
                std::cout << "EXIT_OOM" << std::endl;
                exit(0);
            }
        }

        log_messages.clear();
    }
    virtual void release() = 0;
};
//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------
class WithSample_AllocEmpty_UntilOOM_Benchmark : public IBenchmark
{
public:
    std::string getId() override { return WithSample_AllocEmpty_UntilOOM; }

    void init() override
    {
        logInit();
        max_steps = 40; // only test until xx gb
        step = 0;
        fragShaderProps.SAMPLE_TEXTURES_ENABLED = true;
    }
    void beforeFrame(const uint64_t& frame) override
    {
        const auto step_frequency = 100; // frames between steps

        if (frame == 0)
        {
            logMemory(step, max_steps);
        }
        if (frame % step_frequency == 0)
        {
            step++;
        }
        if (frame % step_frequency == 1)
        {
            allocTexture(false);
        }
        if (frame % step_frequency == 2)
        {
            logMemory(step, max_steps);
        }

        IBenchmark::beforeFrame(frame);
    }
    void afterFrame(const uint64_t& frame) override
    {
        IBenchmark::afterFrame(frame);
    }
    void release() override {}
};
//----------------------------------------------------------------------------------------------------
class WithSample_AllocData_UntilOOM_Benchmark : public IBenchmark
{
public:
    std::string getId() override { return WithSample_AllocData_UntilOOM; }

    void init() override
    {
        logInit();
        max_steps = 40; // only test until xx gb
        step = 0;
        fragShaderProps.SAMPLE_TEXTURES_ENABLED = true;
    }
    void beforeFrame(const uint64_t& frame) override
    {
        const auto step_frequency = 100; // frames between steps

        if (frame == 0)
        {
            logMemory(step, max_steps);
        }
        if (frame % step_frequency == 0)
        {
            step++;
        }
        if (frame % step_frequency == 1)
        {
            allocTexture(true);
        }
        if (frame % step_frequency == 2)
        {
            logMemory(step, max_steps);
        }

        IBenchmark::beforeFrame(frame);
    }
    void afterFrame(const uint64_t& frame) override
    {
        IBenchmark::afterFrame(frame);
    }
    void release() override {}
};
//----------------------------------------------------------------------------------------------------
class NoSample_AllocEmpty_UntilOOM_Benchmark : public IBenchmark
{
public:
    std::string getId() override { return NoSample_AllocEmpty_UntilOOM; }

    void init() override
    {
        logInit();
        max_steps = 100; // only test until xx gb
        step = 0;
        fragShaderProps.SAMPLE_TEXTURES_ENABLED = false;
    }
    void beforeFrame(const uint64_t& frame) override
    {
        const auto step_frequency = 100; // frames between steps

        if (frame == 0)
        {
            logMemory(step, max_steps);
        }
        if (frame % step_frequency == 0)
        {
            step++;
        }
        if (frame % step_frequency == 1)
        {
            allocTexture(false);
        }
        if (frame % step_frequency == 2)
        {
            logMemory(step, max_steps);
        }

        IBenchmark::beforeFrame(frame);
    }
    void afterFrame(const uint64_t& frame) override
    {
        IBenchmark::afterFrame(frame);
    }
    void release() override {}
};
//----------------------------------------------------------------------------------------------------
class NoSample_AllocData_UntilOOM_Benchmark : public IBenchmark
{
public:
    std::string getId() override { return NoSample_AllocData_UntilOOM; }

    void init() override
    {
        logInit();
        max_steps = 40; // only test until xx gb
        step = 0;
        fragShaderProps.SAMPLE_TEXTURES_ENABLED = false;
    }
    void beforeFrame(const uint64_t& frame) override
    {
        const auto step_frequency = 100; // frames between steps

        if (frame == 0)
        {
            logMemory(step, max_steps);
        }
        if (frame % step_frequency == 0)
        {
            step++;
        }
        if (frame % step_frequency == 1)
        {
            allocTexture(true);
        }
        if (frame % step_frequency == 2)
        {
            logMemory(step, max_steps);
        }

        IBenchmark::beforeFrame(frame);
    }
    void afterFrame(const uint64_t& frame) override
    {
        IBenchmark::afterFrame(frame);
    }
    void release() override {}
};
//----------------------------------------------------------------------------------------------------
