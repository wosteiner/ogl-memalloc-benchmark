#pragma once

#include <string>

const auto WithSample_AllocEmpty_UntilOOM = std::string("with-sample_alloc-empty_until-oom");
const auto WithSample_AllocData_UntilOOM = std::string("with-sample_alloc-data_until-oom");
const auto NoSample_AllocEmpty_UntilOOM = std::string("no-sample_alloc-empty_until-oom");
const auto NoSample_AllocData_UntilOOM = std::string("no-sample_alloc-data_until-oom");
