#pragma once
#include <array>
#include <cstdio>
#include <iomanip>
#include <optional>
#include <regex>
#include <sstream>
#include <string>

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#ifdef WIN32
#define pclose _pclose
#define popen _popen
#endif

std::optional<int> process_exit_code = std::nullopt;

void close(FILE* fstream)
{
    process_exit_code = pclose(fstream);
}

std::string exec(const std::string& cmd)
{
    process_exit_code = std::nullopt;

    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&close)> pipe(popen(cmd.c_str(), "r"), close);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

std::vector<std::string> split(const char* str)
{
    std::vector<std::string> result;

    if (!str)
        return result;

    std::stringstream ss(str);
    std::string line;

    while (std::getline(ss, line, '\n'))
        result.push_back(line);

    return result;
}

auto pid = GetCurrentProcessId();

std::string GPU_Process_Memory_Query(const std::string& subQuery, std::optional<LUID> gpuLuid = std::nullopt)
{
    std::stringstream sstr;
    sstr << "\"\\GPU Process Memory(pid_" << pid << "_";

    if (gpuLuid.has_value())
    {
        auto luid = gpuLuid.value();
        sstr << "luid_0x" << std::setfill('0') << std::setw(8) << std::hex << luid.HighPart;
        sstr << "_0x" << std::setfill('0') << std::setw(8) << std::hex << luid.LowPart;
    }

    sstr << "*)\\" << subQuery << "\"";
    return sstr.str();
}

const auto GPU_SharedMemory = GPU_Process_Memory_Query("Shared Usage");
const auto GPU_DedicatedMemory = GPU_Process_Memory_Query("Dedicated Usage");

const auto GPU_SharedMemoryEx = [](LUID luid) { return GPU_Process_Memory_Query("Shared Usage", luid); };
const auto GPU_DedicatedMemoryEx = [](LUID luid) { return GPU_Process_Memory_Query("Dedicated Usage", luid); };

// see: https://github.com/craignicholson/typeperf for examples
// TypePerf.exe -q > counters.txt
const auto CPU_ProcessorTime = "\"\\Processor(_total)\\% Processor Time\"";
const auto CPU_UsedMemory = "\"\\Memory\\Committed Bytes\"";
const auto CPU_AvailableMemory = "\"\\Memory\\Available Bytes\"";

struct SysMemStats
{
    double totalBytes = 0;
    double freeBytes = 0;
    double usedBytes = 0;

    bool empty() const
    {
        return totalBytes == 0 && freeBytes == freeBytes && usedBytes == 0;
    }
};

SysMemStats getSysStats()
{
    SysMemStats result;

    MEMORYSTATUSEX stat;
    ZeroMemory(&stat, sizeof(MEMORYSTATUSEX));
    stat.dwLength = sizeof(stat);

    if (GlobalMemoryStatusEx(&stat))
    {
        result =
        {
            double(stat.ullTotalPhys),
            double(stat.ullAvailPhys),
            double(stat.ullTotalPhys - stat.ullAvailPhys)
        };
    }

    return result;
}

std::vector<double> getHardwareQuery(std::vector<std::string> queries, std::optional<LUID> gpuLuid = std::nullopt)
{
    std::vector<double> query_result;
    std::stringstream sstr;

    sstr << "typeperf ";
    for (auto& query : queries)
        sstr << query << " ";

    sstr << " -sc 1";
    auto output = exec(sstr.str().c_str());

    auto lines = split(output.c_str());

    if (lines.size() != 6)
        return query_result;

    const std::regex rx(R"(\"(\d+\.\d+)\")");
    auto input = lines[2];
    std::smatch result;

    while (std::regex_search(input, result, rx))
    {
        if (result.size() == 2)
        {
            auto d = std::stod(result[1]);
            query_result.push_back(d);
        }

        input = result.suffix().str();
    }

    if (query_result.size() != queries.size())
    {
        query_result.clear();
        return query_result;
    }

    return query_result;
}

const char* memUnitsStr[] = { "Byte", "kByte", "MByte", "GByte" };

auto formatMemory(double& bytesValue, int& unitIdx)
{
    unitIdx = 1;
    while (bytesValue > 1024.0)
    {
        bytesValue /= 1024.0;
        ++unitIdx;
    }
};

auto formatMemory(double bytesValue)
{
    if (bytesValue < 0)
        return std::to_string(bytesValue);

    int unitIdx;
    formatMemory(bytesValue, unitIdx);
    std::stringstream sstr;
    sstr << std::setprecision(2) << std::fixed;
    sstr << bytesValue << " " << memUnitsStr[unitIdx - 1];
    return sstr.str();
}
