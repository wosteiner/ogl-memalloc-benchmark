#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "common.h"
#include "utils.h"

#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3native.h>

#include <iostream>
#include <vector>
#include <string>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);
void allocTexture(const bool& withData);

// settings
const unsigned int fbo_width = 1200;
const unsigned int fbo_height = 300;

std::vector<GLint> textures;
float* tex_data = nullptr;
auto tex_size_px = 16384;

// TESTED TEXTURE FORMATS:

// 32 bit R32F --> uses Shared Video Memory
//constexpr auto texFormat = GL_R32F;
//constexpr auto texLayout = GL_RED;
//constexpr auto texDataType = GL_FLOAT;
//constexpr auto numChannels = 1;

// 32 bit float DEPTH --> crashes as soon as dedicated VRAM is full
constexpr auto texFormat = GL_DEPTH_COMPONENT32F;
constexpr auto texLayout = GL_DEPTH_COMPONENT;
constexpr auto texDataType = GL_FLOAT;
constexpr auto numChannels = 1;

uint64_t frameIdx = 0;
std::vector<std::string> log_messages;
HWND hwnd = 0;

auto vShaderCode = R"(
#version 450 core
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec2 aTexCoord;

out vec2 TexCoord;

void main()
{
    gl_Position = vec4(aPos, 1.0);
    TexCoord = vec2(aTexCoord.x, aTexCoord.y);
}
)";

struct FsShaderProps
{
    bool SAMPLE_TEXTURES_ENABLED = false;
    uint32_t NUM_TEXTURES = 0;

    std::string getDefines() const
    {
        std::stringstream sstr;
        sstr << "#define SAMPLE_TEXTURES_ENABLED " << (SAMPLE_TEXTURES_ENABLED ? "true" : "false") << std::endl;
        sstr << "#define NUM_TEXTURES " << NUM_TEXTURES << std::endl;
        return sstr.str();
    }
};

inline bool operator==(const FsShaderProps& a, const FsShaderProps& b)
{
    return a.NUM_TEXTURES == b.NUM_TEXTURES && a.SAMPLE_TEXTURES_ENABLED == b.SAMPLE_TEXTURES_ENABLED;
}

FsShaderProps fragShaderProps;
auto fShaderCode = R"(
out vec4 FragColor;

in vec2 TexCoord;

// texture sampler
uniform sampler2D texture1[NUM_TEXTURES];

void main()
{
    FragColor = vec4(0,1,0,1);

    if (SAMPLE_TEXTURES_ENABLED)
    {
        for (int i=0; i<NUM_TEXTURES; ++i)
            FragColor.rgb += textureLod(texture1[i], TexCoord, 0).rgb;
    }
    else
    {
        FragColor = vec4(1,0,0,1);
    }
}
)";

void checkCompileErrors(unsigned int shader, std::string type)
{
    int success;
    char infoLog[1024];
    if (type != "PROGRAM")
    {
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(shader, 1024, NULL, infoLog);
            std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
        }
    }
    else
    {
        glGetProgramiv(shader, GL_LINK_STATUS, &success);
        if (!success)
        {
            glGetProgramInfoLog(shader, 1024, NULL, infoLog);
            std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
        }
    }
}

int vertex = 0;
int fragment = 0;
int program = 0;

void updateShader()
{
    static FsShaderProps currProps;

    if (currProps == fragShaderProps)
        return;

    currProps = fragShaderProps;

    glDeleteShader(vertex);
    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, NULL);
    glCompileShader(vertex);
    checkCompileErrors(vertex, "VERTEX");
    // fragment Shader
    glDeleteShader(fragment);
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    auto fsDefines = fragShaderProps.getDefines();
    std::string fs = "#version 450 core\n" + fsDefines + "\n" + fShaderCode;
    auto fs_str = fs.c_str();
    glShaderSource(fragment, 1, &fs_str, NULL);
    glCompileShader(fragment);
    checkCompileErrors(fragment, "FRAGMENT");
    // shader Program
    glDeleteProgram(program);
    program = glCreateProgram();
    glAttachShader(program, vertex);
    glAttachShader(program, fragment);
    glLinkProgram(program);
    checkCompileErrors(program, "PROGRAM");
    // delete the shaders as they're linked into our program now and no longer necessary
    glDeleteShader(vertex);
    glDeleteShader(fragment);
}

#include "benchmarks.h"

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        std::cout << "ERROR: invalid arguments" << std::endl;
        return -1;
    }

    IBenchmark* benchmark = nullptr;

#define LoadBenchmark(name) \
    else if (argv[1] == name) \
        benchmark = new name##_Benchmark()

    if (false) {}
    LoadBenchmark(WithSample_AllocEmpty_UntilOOM);
    LoadBenchmark(WithSample_AllocData_UntilOOM);
    LoadBenchmark(NoSample_AllocEmpty_UntilOOM);
    LoadBenchmark(NoSample_AllocData_UntilOOM);

    if (!benchmark)
    {
        std::cout << "ERROR: unknown benchmark id" << std::endl;
        return -2;
    }

    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(fbo_width, fbo_height, "benchmark.exe", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSwapInterval(0);
    hwnd = glfwGetWin32Window(window);

    const auto glfwErrorCallback = [](GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const GLvoid *userParam)
    {
        const std::string expected = "Texture state usage warning: The texture object (0) bound to texture image unit 0 does not have a defined base level and cannot be used for texture mapping.";

        if (message == expected)
            return;

        if (std::string(message).find("Buffer detailed info: Buffer object") == 0) // nvidia
            return;

        log_messages.push_back(message);
    };

    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }

    tex_data = new float[tex_size_px * tex_size_px * numChannels];
    for (int x = 0; x < tex_size_px * tex_size_px; ++x)
        for (int c = 0; c < numChannels; ++c)
            tex_data[x * numChannels + c] = sin(x * 0.001);

    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(glfwErrorCallback, nullptr);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, 1);

    // set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    float vertices[] = {
        // positions          // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 1.0f, // top right
         0.5f, -0.5f, 0.0f,   1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f,   0.0f, 1.0f  // top left 
    };
    unsigned int indices[] = {  
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };
    unsigned int VBO, VAO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // texture coord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    GLint max_frag_units;
    glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &max_frag_units);
    if (glGetError() != GL_NO_ERROR)
    {
        std::cout << "ERROR: unable to get GL max frag texture units" << std::endl;
        exit(-3);
    }

    benchmark->init();

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        benchmark->beforeFrame(frameIdx);

        // input
        // -----
        processInput(window);

        // render
        // ------
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        int numTextures = textures.size();
        int numDraws = std::ceil(numTextures / double(max_frag_units));

        for (int i = 0; i < numDraws; ++i)
        {
            auto texBegin = i * max_frag_units;
            auto texEnd = std::min(texBegin + max_frag_units, numTextures);

            fragShaderProps.NUM_TEXTURES = std::max(1, texEnd - texBegin);

            updateShader();
            glUseProgram(program);

            int texSlot = 0;
            for (auto x = texBegin; x < texEnd; ++x)
            {
                glActiveTexture(GL_TEXTURE0 + texSlot++);
                auto& t = textures[x];
                glBindTexture(GL_TEXTURE_2D, t);

                auto uniform = "texture1[" + std::to_string(texSlot) + "]";
                auto loc = glGetUniformLocation(program, uniform.c_str());
                glUniform1i(loc, texSlot);
            }

            glBindVertexArray(VAO);
            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        }

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();

        benchmark->afterFrame(frameIdx);

        frameIdx++;
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    delete[] tex_data;
    tex_data = nullptr;

    benchmark->release();

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

void allocTexture(const bool& withData)
{
    unsigned int texture;
    glGenTextures(1, &texture);
    textures.push_back(texture);

    glBindTexture(GL_TEXTURE_2D, texture); // all upcoming GL_TEXTURE_2D operations now have effect on this texture object
    // set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// set texture wrapping to GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    //glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, sizePx, sizePx, 0, GL_DEPTH_COMPONENT, GL_FLOAT, withData ? tex_data : nullptr);
    //glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, sizePx, sizePx, 0, GL_RED, GL_FLOAT, data);
    glTexImage2D(GL_TEXTURE_2D, 0, texFormat, tex_size_px, tex_size_px, 0, texLayout, texDataType, withData ? tex_data : nullptr);
    //glGenerateMipmap(GL_TEXTURE_2D);
}
