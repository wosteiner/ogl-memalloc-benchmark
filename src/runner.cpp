#include "common.h"
#include "utils.h"

#include <fstream>
#include <iostream>

void runBenchmark(const std::string& benchmarkId)
{
    std::cout << "STARTING: " << benchmarkId << std::endl;

    auto output = exec("benchmark.exe " + benchmarkId);
    std::ofstream ofs(benchmarkId + ".csv");

    if (!ofs.good() || !ofs.is_open())
    {
        std::cout << "ERROR: Unable to write log: " << benchmarkId << std::endl;
        exit(-1);
    }

    ofs.write(output.c_str(), output.size());

    if (process_exit_code.value_or(0))
    {
        ofs << "EXIT_CRASH" << std::endl;
    }

    ofs.close();

    std::cout << "FINISHED: " << benchmarkId << std::endl;
}

int main(int argc, char** argv)
{
    runBenchmark(WithSample_AllocEmpty_UntilOOM);
    runBenchmark(WithSample_AllocData_UntilOOM);
    runBenchmark(NoSample_AllocEmpty_UntilOOM);
    runBenchmark(NoSample_AllocData_UntilOOM);
    return 0;
}
