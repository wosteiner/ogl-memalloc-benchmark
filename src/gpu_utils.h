#pragma once

#include <algorithm>
#include <string>

#include <GL/glew.h>
#include <GL/wglew.h>

enum class VendorId
{
    Unknown = 0,
    Nvidia,
    Amd,
    Intel
};

LUID getGlContextGpuLuid()
{
    GLubyte data[GL_LUID_SIZE_EXT];
    glGetUnsignedBytevEXT(GL_DEVICE_LUID_EXT, data);
    if (glGetError() != GL_NO_ERROR)
    {
        std::cout << "ERROR: unable to get LUID for GPU adapter" << std::endl;
        exit(-4);
    }
    LUID luid = *(LUID*)&data;
    return luid;
}

std::string getGpuVendorName()
{
    const auto vendor = (const char*)glGetString(GL_VENDOR);
    if (vendor)
        return vendor;

    return "";
}

VendorId getGpuVendor()
{
    std::string name = getGpuVendorName();
    std::transform(name.begin(), name.end(), name.begin(), ::tolower);

    if (name.find("nvidia") != std::string::npos) return VendorId::Nvidia;
    if (name.find("amd") != std::string::npos) return VendorId::Amd;
    if (name.find("ati") != std::string::npos) return VendorId::Amd;
    if (name.find("intel") != std::string::npos) return VendorId::Intel;

    return VendorId::Unknown;
}

size_t getTotalVRAM()
{
    size_t amount = -1;
    VendorId vendor = getGpuVendor();

    if (vendor == VendorId::Amd)
    {
        if (WGL_AMD_gpu_association)
        {
            UINT n = wglGetGPUIDsAMD(0, 0);
            UINT *ids = new UINT[n];
            size_t totalMegabytes = 0;
            wglGetGPUIDsAMD(n, ids);
            wglGetGPUInfoAMD(ids[0], WGL_GPU_RAM_AMD, GL_UNSIGNED_INT, sizeof(size_t), &totalMegabytes);

            amount = totalMegabytes * 1024;
        }
    }
    else if (vendor == VendorId::Nvidia)
    {
        if (GLEW_NVX_gpu_memory_info)
        {
            GLint totalRam = 0;
            glGetIntegerv(GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX, &totalRam);
            amount = totalRam;
        }
    }

    return amount;
}
